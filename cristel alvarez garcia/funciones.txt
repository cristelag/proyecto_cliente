function menu() {
    var op = parseInt(prompt("1.Altas\n2.Vacunar\n3.Bajas\n4.Cerrar\nElija una opción:"));
    switch (op) {
        case 1:
            alta();
            break;
        case 2:
            vacunar();
            break;
        case 3:
            baja();
            break;
        case 4:
            break;
        default:
            alert("Opción incorrecta.");
            break;
    }
}